# Restify

## Table of Contents

* [Dependencies](#dependencies)
* [Installing](#installing)
* [Running the app](#running)

## Dependencies

What you need to run this app:
* `node` and `npm`
* Ensure you're running Node (`v4.4.x`+) and NPM (`3.9.x`+)

## Installing

* `npm install` to install all dependencies

## Running

After you have installed all dependencies you can now run the app with:
```
npm start
```

It will start a restify server at `http://localhost:8080`. 
